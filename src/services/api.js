import axios from 'axios';

const api = axios.create({
  baseURL: 'https://20d64a96.ngrok.io/',
});

export default api;
