import { call, put } from 'redux-saga/effects';
import PodcastActions from '../ducks/podcasts';
import api from '~/services/api';

export function* load() {
  try {
    const response = yield call(api.get, 'podcasts');
    yield put(PodcastActions.loadSuccess(response.data));
  } catch (error) {
    yield put(PodcastActions.loadFailure());
  }
}
