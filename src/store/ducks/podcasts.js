import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

// Action Types & Creators

const { Types, Creators } = createActions({
  loadRequest: null,
  loadSuccess: ['data'],
  loadFailure: null,
});

export const PodcastTypes = Types;
export default Creators;

// Types: { LOAD_REQUEST, LOAD_SUCESS, LOAD_FAILURE }

// Creators:
/**
 * loadSuccess: () => { type: 'LOAD_SUCCESS' },
 * loadRequest: (data) => { type: 'LOAD_REQUEST', data },
 * loadFailure: () => { type: 'LOAD_FAILURE' },
 */

// Initial state

const INITIAL_STATE = Immutable({
  data: [],
});

// reducer

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOAD_SUCCESS]: (state, { data }) => state.merge({ data }),
});
